﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerBrand
/// </summary>
public class ControllerBrand : ClassBase
{
    public ControllerBrand(DataClassesDatabaseDataContext _db) : base (_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Untuk menampilkan data & get data
    public TBBrand[] Data()
    {
        return db.TBBrands.ToArray();
    }

    //Create data
    public TBBrand Create(string name, string address, string telephone, string email)
    {
        TBBrand Brand = new TBBrand
        {
            Name = name,
            Address = address,
            Telephone = telephone,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now,
            Email = email
        };

        db.TBBrands.InsertOnSubmit(Brand);

        return Brand;
    }

    //Search data
    public TBBrand Cari(string UID)
    {
        return db.TBBrands.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    //Update
    public TBBrand Update(string UID, string name, string address, string telephone, string email)
    {
        var Brand = Cari(UID);

        if (Brand != null)
        {
            Brand.Name = name;
            Brand.Address = address;
            Brand.Telephone = telephone;
            Brand.Email = email;

            return Brand;
        }
        else
            return null;
    }

    //Delete
    public TBBrand Delete(string UID)
    {
        var Brand = Cari(UID);

        if (Brand != null)
        {
            db.TBBrands.DeleteOnSubmit(Brand);
            db.SubmitChanges();

            return Brand;
        }
        else
            return null;
    }
}