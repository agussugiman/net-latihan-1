﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Channels;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using System.Threading.Tasks;

/// <summary>
/// Summary description for ControllerCompany
/// </summary>
public class ControllerCompany : ClassBase
{
    public ControllerCompany(DataClassesDatabaseDataContext _db) : base (_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Untuk menampilkan data & get data
    public TBCompany[] Data()
    {
        return db.TBCompanies.ToArray();
    }

    //Create data
    public TBCompany Create(string name, string address, string telephone)
    {
        TBCompany company = new TBCompany
        {
            Name = name,
            Address = address,
            Telephone = telephone,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now
        };

        db.TBCompanies.InsertOnSubmit(company);

        return company;
    }

    //Search data
    public TBCompany Cari(string UID)
    {
        return db.TBCompanies.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    //Update
    public TBCompany Update(string UID, string name, string address, string telephone)
    {
        var company = Cari(UID);

        if (company != null)
        {
            company.Name = name;
            company.Address = address;
            company.Telephone = telephone;

            return company;
        }
        else
            return null;
    }

    //Delete
    public TBCompany Delete(string UID)
    {
        var company = Cari(UID);

        if (company != null)
        {
            db.TBCompanies.DeleteOnSubmit(company);
            db.SubmitChanges();

            return company;
        }
        else
            return null;
    }

    public void DropDownListCompany(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> company = new List<ListItem>();

        company.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        company.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));

        return company.ToArray();
    }

    //public async Task<string> GetDataFromMiddleware(string data)
    //{
    //    RestClient client = new RestClient("http://119.235.251.242:91");
    //    RestRequest request = new RestRequest("API/webservice.asmx/GetDataCompany", Method.Get);
    //    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
    //    request.AddParameter("IDWMSTempat", data);
    //    var cancellationTokenSource = new CancellationTokenSource();
    //    var response = await client.ExecuteGetAsync<ModelResult>(request, cancellationTokenSource.Token);
    //    var res = response.Data.Result.Pesan;
    //    return res.ToString();
    //}

    public string GetDataFromMiddleware(string data)
    {
        RestClient client = new RestClient("http://119.235.251.242:91");

        RestRequest request = new RestRequest("API/webservice.asmx/GetDataCompany", Method.Get);

        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

        var cancellationTokenSource = new CancellationTokenSource();
        var response = client.Execute<ModelResult>(request);

        var res = response.Data.Result.Pesan;
        return res.ToString();
    }
}