﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerUser
/// </summary>
public class ControllerUser : ClassBase
{
    public ControllerUser(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void ViewData()
    {
        db.TBUsers.Select(x => new
        {
            x.ID,
            x.IDCompany,
            NameCompany = x.TBCompany.Name,
            x.Address,
            x.Email,
            x.Telephone
        }).ToArray();
    }

    public TBUser Create(int IDCompany, string name, string address, string telephone, string email)
    {
        TBUser user = new TBUser
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            Name = name,
            Address = address,
            Telephone = telephone,
            Email = email,
            CreatedAt = DateTime.Now
        };

        db.TBUsers.InsertOnSubmit(user);

        return user;
    }

    public TBUser Cari(int ID)
    {
        return db.TBUsers.FirstOrDefault(x => x.ID == ID);
    }

    public TBUser Update(int ID, int IDCompany, string name, string address, string telephone, string email)
    {
        var user = Cari(ID);

        if (user != null)
        {
            user.IDCompany = IDCompany;
            user.Name = name;
            user.Address = address;
            user.Email = email;
            user.Telephone = telephone;

            return user;
        }
        else
            return null;
    }
}