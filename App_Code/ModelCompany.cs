﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ModelCompany
/// </summary>
[Serializable]
public partial class ModelCompany
{
    public List<ModelCompany> Data { get; set; }
}

public partial class ModelCompany
{
    public string GUID { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
    public string Telephone { get; set; }
    public DateTime CreateAt { get; set; }
}