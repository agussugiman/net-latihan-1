﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public void Company()
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                var companys = db.TBCompanies.Select(x => new
                {
                    ID = x.ID,
                    UID = x.UID,
                    Name = x.Name,
                    x.Address,
                    x.Telephone,
                    x.CreatedAt
                }).ToArray();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = companys,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("[Error] ") ? ex.Message : "Terjadi kesalahan"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void Brand()
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                var brands = db.TBBrands
                    .ToArray();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = brands,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("[Error] ") ? ex.Message : "Terjadi kesalahan"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void CompanybyUID(string uid)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                var companies = db.TBCompanies.Where(x => x.UID.ToString() == uid)
                    .Select(x => new
                    {
                        ID = x.ID,
                        UID = x.UID,
                        Name = x.Name,
                        x.Address,
                        x.Telephone,
                        x.CreatedAt
                    }).ToArray();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = companies,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Companies = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("[Error] ") ? ex.Message : "Terjadi kesalahan"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void CreateCompany(string name, string address, string telephone)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);

                var companys = controllerCompany.Create(name, address, telephone);

                db.SubmitChanges();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = companys,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("[Error] ") ? ex.Message : "Terjadi kesalahan"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void UpdateCompany(string uid, string name, string address, string telephone)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);

                var company = controllerCompany.Cari(uid);

                if (company != null)
                {
                    var companys = controllerCompany.Update(uid, name, address, telephone);

                    db.SubmitChanges();

                    Context.Response.Write(JsonConvert.SerializeObject(new
                    {
                        Data = companys,
                        Result = new WebServiceResult
                        {
                            EnumWebService = (int)EnumWebService.Success,
                            Pesan = "success"
                        }
                    }, Formatting.Indented));
                }
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("[Error] ") ? ex.Message : "Terjadi kesalahan"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void DeleteCompany(string uid)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);

                var company = controllerCompany.Cari(uid);

                if (company != null)
                {
                    var companys = controllerCompany.Delete(uid);

                    db.SubmitChanges();

                    Context.Response.Write(JsonConvert.SerializeObject(new
                    {
                        Data = companys,
                        Result = new WebServiceResult
                        {
                            EnumWebService = (int)EnumWebService.Success,
                            Pesan = "success"
                        }
                    }, Formatting.Indented));
                }
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("[Error] ") ? ex.Message : "Terjadi kesalahan"
                }
            }, Formatting.Indented));
        }
    }
}
