﻿using ImageResizer.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Brand_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }
        }
    }
   
    //Menampilkan data
    private void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerBrand controllerBrand = new ControllerBrand(db);

        repeaterBrand.DataSource = controllerBrand.Data();
        repeaterBrand.DataBind();
    }

    //Fungsi untuk update dan delete dalam table repeater
    protected void repeaterBrand_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Brand/Form.aspx?uid=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var Brand = db.TBBrands.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());
               
                db.TBBrands.DeleteOnSubmit(Brand);
                db.SubmitChanges();

                LoadData(db);
            }
        }
    }
}