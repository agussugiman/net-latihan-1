﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Brand_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerBrand controllerBrand = new ControllerBrand(db);

                var Brand = controllerBrand.Cari(Request.QueryString["uid"]);

                if (Brand != null)
                {
                    InputName.Text = Brand.Name;
                    InputAddress.Text = Brand.Address;
                    InputTelephone.Text = Brand.Telephone;
                    InputEmail.Text = Brand.Email;

                    ButtonOk.Text = "Update";
                    LabelTitle.Text = "Update Brand";
                }
                else
                {
                    ButtonOk.Text = "Add New";
                    LabelTitle.Text = "Add New Brand";
                }
            }
        }
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerBrand controllerBrand = new ControllerBrand(db);

                if (ButtonOk.Text == "Add New")
                    controllerBrand.Create(InputName.Text, InputAddress.Text, InputTelephone.Text, InputEmail.Text);
                else if (ButtonOk.Text == "Update")
                    controllerBrand.Update(Request.QueryString["uid"], InputName.Text, InputAddress.Text, InputTelephone.Text, InputEmail.Text);

                db.SubmitChanges();

                Response.Redirect("/Brand/Default.aspx");
            }
        }
    }

    protected void ButtonKeluar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Brand/Default.aspx");
    }
}