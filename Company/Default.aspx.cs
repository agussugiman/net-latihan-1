﻿using ImageResizer.ExtensionMethods;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }
        }
    }
   
    //Menampilkan data
    private void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerCompany controllerCompany = new ControllerCompany(db);

        repeaterCompany.DataSource = controllerCompany.Data();
        repeaterCompany.DataBind();
    }

    //Fungsi untuk update dan delete dalam table repeater
    protected void repeaterCompany_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Company/Form.aspx?uid=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var company = db.TBCompanies.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());
               
                db.TBCompanies.DeleteOnSubmit(company);
                db.SubmitChanges();

                LoadData(db);
            }
        }
    }

    protected void btnGetData_Click(object sender, EventArgs e)
    {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);

            var configData = controllerCompany.GetDataFromMiddleware("1");

            var Result = JsonConvert.DeserializeObject<ModelCompany>(configData);

                foreach (var DataServer in Result.Data)
                {
                    var DataLocal = db.TBCompanies
                        .FirstOrDefault(item => item.Name.ToString() == DataServer.Name);

                    if (DataLocal == null)
                    {
                        db.TBCompanies.InsertOnSubmit(new TBCompany
                        {
                            UID = Guid.NewGuid(),
                            Name = DataServer.Name,
                            Address = DataServer.Address,
                            Telephone = DataServer.Telephone,
                            CreatedAt = DateTime.Now
                        });
                    }
                    else
                    {
                        DataLocal.Name = DataServer.Name;
                        DataLocal.Address = DataServer.Address;
                        DataLocal.Telephone = DataServer.Telephone;
                    }

                    db.SubmitChanges();
                }
            }
    }
}