﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);
                listCompany.Items.AddRange(controllerCompany.DropDownList());

                ControllerUser controllerUser = new ControllerUser(db);
                var user = controllerUser.Cari(Request.QueryString["id"].ToInt());

                if (user != null)
                {
                    listCompany.SelectedValue = user.IDCompany.ToString();
                    InputName.Text = user.Name;
                    InputAddress.Text = user.Address;
                    InputTelephone.Text = user.Telephone;
                    InputEmail.Text = user.Email;

                    btnOk.Text = "Update";
                    LabelTitle.Text = "Update User";
                }
                else
                {
                    btnOk.Text = "Add New";
                    LabelTitle.Text = "Add New User";
                }
            }
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerUser controllerUser = new ControllerUser(db);

                if (btnOk.Text == "Add New")
                {
                    controllerUser.Create(int.Parse(listCompany.SelectedValue), InputName.Text, 
                        InputAddress.Text, InputTelephone.Text, InputEmail.Text);
                }
                else if (btnOk.Text == "Update")
                    controllerUser.Update(Request.QueryString["id"].ToInt(), int.Parse(listCompany.SelectedValue), 
                        InputName.Text, InputAddress.Text, InputTelephone.Text, InputEmail.Text);

                db.SubmitChanges();

                Response.Redirect("/User/Default.aspx");
            }
        }
    }
}